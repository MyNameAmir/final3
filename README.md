# Install and Build

```shell
$ cd frontend
$ yarn install
$ yarn build
$ cd ../backend
$ yarn install
```

# Run

Set your `PORT` and `MONGODBURI` environment variables.

From the `backend` directory:

```shell
$ yarn run dev
```
