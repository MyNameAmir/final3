import { Application } from '../declarations';
import urls from './urls/urls.service';
import checkurls from './checkurls/checkurls.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(urls);
  app.configure(checkurls);
}
