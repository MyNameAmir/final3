// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import htmlparser2 from 'htmlparser2';
import axios from 'axios';
import nodemailer from 'nodemailer';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { app, result } = context;
    const urlsService = app.service( 'urls' );
    let inAnchor = false;
    let outBoundURLArray:any = [];
    console.log( result );

    urlsService.find({
      query:{
        url: result.url
      }
    });

    if(urlsService.total > 0){
      result.count++;
    }
    else{
      result.count = 1;
    }


    const parser = new htmlparser2.Parser({
      onopentag(name, attribs) {
        if (name === 'a') {
          inAnchor = true;
        }
      },
      ontext(outboundURL) {
            if (inAnchor) {
                outBoundURLArray.push(outboundURL);
            }
            inAnchor = false;
        }
    },
     { decodeEntities: true }
    );

    axios.get(result.url)
    .then( response => {
        parser.write( response.data );
        parser.end();
      })
      .catch( error => {
        console.log("Could not fetch page.");
    });



    let transporter = nodemailer.createTransport({
      host: "smtp.sendgrid.net",
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: "apikey",  // SendGrid user
        pass: process.env.SENDGRID_PASSWORD // SendGrid password
      }
    });

    let info = await transporter.sendMail({
      from: '"No Reply" <noreply@494914.xyz>', // PUT YOUR DOMAIN HERE
      to: result.to, // list of receivers
      subject: "Hello " + result.id, // Subject line
      text: outBoundURLArray // plain text body
    });





    context.result = {
      url: result.url,
      email: result.email,
      count: result.count,
      outBoundURLArray: outBoundURLArray
    };
    return context;
  };
};
