import React, { Component } from 'react';

class TopUrls extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const { urlsService } = this.props;

    urlsService.find({
        query: {
            $limit: 10
        }
    }).then(tenUrls => {
        const urls = tenUrls.data
        .sort((a, b) => a.count > b.count)
        .reverse();

        this.setState({urlsService, urls});
    }).catch(err => {
        console.log(err);
    })
  }

  render() {
    return(
      <div>
        <h3 className="mt-4">Top 10 URLs Checked</h3>

        <table id="urls" class="table">
          <thead>
            <tr>
              <th scope="col">Count</th>
              <th scope="col">URL</th>
            </tr>
          </thead>
          <tbody>
              {this.state.urls && this.state.urls.map(url => <tr key={url._id}>
                  <th> {url.url} </th>
                  <td> {url.count} </td>
              </tr>)}
          </tbody>
        </table>

      </div>
    );
  }
}

export default TopUrls;
