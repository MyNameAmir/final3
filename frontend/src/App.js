import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Home from './Home';
import CheckUrl from './CheckUrl';
import TopUrls from './TopUrls';

import client from './feathers';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const urlsService = client.service('urls');
    const checkurlsService = client.service('checkurls');
    this.setState( {urlsService, checkurlsService});
  }

  render() {
    return(
      <Router>
        <div className="container">
          <h1>Outbound Link Checker</h1>

          <div className="list-group">
            <Link to="/check-url" className="list-group-item list-group-item-action">Check URL</Link>
            <Link to="/top-urls" className="list-group-item list-group-item-action">Top 10 URLs Checked</Link>
          </div>

          <Switch>
            <Route path="/check-url">
              <CheckUrl checkurlsService={this.state.checkurlsService}/>
            </Route>
            <Route path="/top-urls">
              <TopUrls urlsService={this.state.urlsService}/>
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>

        </div>
      </Router>
    );
  }
}

export default App;
