import React, { Component } from 'react';

class CheckUrl extends Component {
  constructor(props) {
    super(props);

    this.state = {
      url: '',
      email: '',
      formClass: ''
    };

    this.handleUrlChange = this.handleUrlChange.bind( this );
    this.handleEmailChange = this.handleEmailChange.bind( this );
    this.handleSubmit = this.handleSubmit.bind( this );
  }

  handleUrlChange( event ) {
    this.setState( {url: event.target.value});
  }

  handleEmailChange( event ) {
    this.setState( {email: event.target.value});
  }

  handleSubmit( event ) {
    const { checkurlsService } = this.props;

    event.preventDefault();

    if ( event.target.checkValidity() ) {
      console.log( this.state );
        checkurlsService.create({
            url: this.state.url,
            email: this.state.email, 
            count: 0            
        })
        .then(() => {
            this.setState({
                formClass: 'form-control is-valid'
            })
        }).catch(error => {
        this.setState({
          url: '',
          email: '',
          formClass: 'form-control is-invalid'
        });
    
});

    } else {
      this.setState( {formClass: 'was-validated'})
    }
}

  render() {
      const { checkurlsService } = this.props;

      checkurlsService.find({
                     query: {
                         url: this.state.url
                     }
                 });
   
    return(
      <div>
        <h3 className="mt-4">Check URL</h3>

        <div className="row">
          <div className="col-md-12 order-md-1">
            <form onSubmit={this.handleSubmit} className={this.state.formClass} noValidate>

              <div className="mb-3">
                <label for="url">URL</label>
                <input
                  type="url"
                  className="form-control"
                  id="url"
                  placeholder=""
                  value={this.state.url}
                  required
                  onChange={this.handleUrlChange} />
                <div className="invalid-feedback">
                  Valid URL is required.
                </div>
              </div>

              <div className="mb-3">
                <label for="email">Your email address</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  placeholder=""
                  value={this.state.email}
                  required
                  onChange={this.handleEmailChange}/>
                <div className="invalid-feedback">
                  Valid email address is required.
                </div>
              </div>

              <button className="btn btn-primary" type="submit">Check URL</button>

            </form>
          </div>
        </div>

        <h4 className="mt-4" id="outboundtitle">Outbound Links</h4>
        <ul id="links" className="list-group">
            {this.state.url && checkurlsService.outBoundURLArray.map (url => 
                checkurlsService.outBoundURLArray.forEach(outboundURL => {
                    <li>{outboundURL}</li>
                }),

            )}
        </ul>
      </div>
    );
  }
}

export default CheckUrl;
